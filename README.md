# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Instruções do jogo

	* 1.Neste jogo haverá 2 jogadores, um contra o outro.
	* 2. Cada jogador terá doze embarcações.
	* 3. Cada embarcação terá uma classicação entre as três possíveis (canoa, submarino, porta-aviões):
	*   3.1. Canoa -> ocupa um espaço no mapa e não tem habilidades especiais.
	*   3.2. Submario -> ocupa dois espaços no mapa e precisa ser atacado duas vezes em cada espaço para ser destruido.
	*   3.3. Porta-aviões -> ocupa quatro espaços no mapa e tem 50% de chance se de defender de a cada ataque.
	* 4. Cada embarcação terá uma orientação (cima, baixo, esquerda, direita), exceto a canoa que teria como orientação a palavra 'nada'.
	* 5. Cada embarcação deverá ser colocada seguindo o seguinte padrão:
	*   5.1. A coordenada x, a coordenada y, a classificação da embarcação e a orientação da embarcação. Exemplo: 2 3 canoa nada.
	* 6. Cada jogador terá um mapa 13X13 (0 - 12) X (0 - 12).
	* 7. Ao realizar o ataque, o jogador deverá colocar o número da coluna e o número da linha (separados por um espaço) que deseja atacar.
	*   7.1. O jogador que tentar atacar quaisquer coordenadas fora dos limites estipulados, perderá a vez.
	* 8. O jogador que tentar colocar uma embarcação fora dos limites estipulados (0 - 12) X (0 - 12), ficará sem essa embarcação.
	* 9. O jogador que tentar colocar uma embarcação em cima de outra, ficará sem a mais nova entre as duas.
	* 10. Ganha o jogador que destruir as 12 embarcações inimigas primeiro.
	* 11. Ganha muito mais quem se diverte.
	
&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

## Contatos

* Rodrigo Tiago (principal e único programador): 
	Email: rodrigotiagocl@gmail.com 
	Telegram: @RodrigoTLima