#ifndef CANOA_HPP
#define CANOA_HPP
#include <iostream>
#include <utility>
#include <string>
#include "embarcacao.hpp"
class Canoa : public Embarcacao{
    private:
    int x;
    int y;
		int vida;
    public:
    Canoa(int x, int y, string tipo, string posicao);
    Canoa();
    ~Canoa();
		void setVida(int x);
		int getVida();
    bool ataque(int x, int y);
};
#endif