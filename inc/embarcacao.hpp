#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP
#include <iostream>
#include <utility>
#include <string>
using namespace std;
class Embarcacao{
	private:
		int x;
		int y;
		string tipo;
		string posicao;
	public:
		Embarcacao();
		Embarcacao(int x, int y, string tipo, string posicao);
		~Embarcacao();
		int getCooX();
		void setCooX(int x);
		int getCooY();
		void setCooY(int y);
		string getTipo();
		void setTipo(string tipo);
		string getPosicao();
		void setPosicao(string posicao);

		virtual void ataque();
};
#endif
