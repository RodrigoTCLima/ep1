#ifndef JOGO_HPP
#define JOGO_HPP
#include <fstream>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include "player.hpp"
#include "canoa.hpp"
#include "pa.hpp"
#include "embarcacao.hpp"
#include "submarino.hpp"
#include "menu.hpp"
#include "mapa.hpp"
using namespace std;
class Jogo{
	public:
		Jogo();
		~Jogo();
		void Leitura();
		void playGame();
		void GameOver(Player x, Player y);
};
#endif