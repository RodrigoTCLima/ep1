#ifndef MAPA_HPP
#define MAPA_HPP
#include <iostream>
#include <vector>
using namespace std;
class Mapa{
	private:
		vector<char> mapa[13];
	public:
		Mapa();
		~Mapa();
		void setInicio(int x, int y);
		void mostrarMapa();
		void setVazio(int x, int y);
		void setSub(int x, int y);
		void setCPA(int x, int y);
};
#endif