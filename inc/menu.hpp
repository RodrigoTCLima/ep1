#ifndef MENU_HPP
#define	MENU_HPP
#include <iostream>
using namespace std;
class Menu{
	private:
	public:
		Menu();
		~Menu();
		int mostrarMenu();
		int telaInicial();
		int mostrarMenuInv();
		int mostrarInstrucoes();
		int mostrarCreditos();
};
#endif