#ifndef PA_HPP
#define PA_HPP
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "embarcacao.hpp"

class PA : public Embarcacao{
	private:
		int x2; 
		int y2; 
		int x3; 
		int y3; 
		int x4; 
		int y4; 
		
		int vida;
		int vida2;
		int vida3;
		int vida4;
		bool destruido;
	public:
		PA(int x, int y, string tipo, string posicao);
		~PA();
		PA();
		bool ataque(int x, int y);
		int getCooX2();
		void setCooX2(int x);
		int getCooY2();
		void setCooY2(int y);
		int getCooX3();
		void setCooX3(int x);
		int getCooY3();
		void setCooY3(int y);
		int getCooX4();
		void setCooX4(int x);
		int getCooY4();
		void setCooY4(int y);
		int getVida();
		void setVida(int vida);
		int getVida2();
		void setVida2(int vida2);
		int getVida3();
		void setVida3(int vida3);
		int getVida4();
		void setVida4(int vida4);
		bool getDestruido();
		void setDestruido(bool destruido);
};
#endif
