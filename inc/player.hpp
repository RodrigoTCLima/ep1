#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <iostream>
#include <cstddef> 
#include <cstdlib>
#include <cstring> 
#include <cwchar> 
#include <vector>
#include <ctime> 
#include <clocale> 
#include <cstdio>
#include <bits/stdc++.h>
#include "mapa.hpp"
#include "embarcacao.hpp"
using namespace std;
class Player{
	private:
		string nome;
		int pontuacao;
		int acertos;
		int tentativas;
		int acePA;
		int falPA;
	public:
		Player(string nome);
		~Player();
		string getNome();
		void setNome(string nome);
		int getPontuacao();
		void setPontuacao(int pontuacao);
		int getAcertos();
		void setAcertos(int acertos);
		int getTentativas();
		void setTentativas(int tentativas);
		int getAcePA();
		void setAcePA(int acePA);
		int getFalPA();
		void setFalPA(int falPA);
};
#endif
