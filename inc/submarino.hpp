#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP
#include "embarcacao.hpp"
class Submarino : public Embarcacao{
	private:
		int x2; //coordenadas
		int y2;
		int vida2;
		int vida;
		bool destruido;
	public:
		Submarino(int x, int y, string tipo, string posicao);
		Submarino();
		~Submarino();
		bool ataque(int x, int y);
		int getCooX2();
		void setCooX2(int x);
		int getCooY2();
		void setCooY2(int y);
		int getVida();
		void setVida(int vida);
		int getVida2();
		void setVida2(int vida2);
		bool getDestruido();
		void setDestruido(bool destruido);
};
#endif