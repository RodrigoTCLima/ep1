#include <iostream>
#include <utility>
#include <string>

#include "canoa.hpp"
Canoa::Canoa(){}
Canoa::Canoa(int x, int y, string tipo, string posicao){
    setCooX(x);
    setCooY(y);
    setTipo(tipo);
    setPosicao(posicao);
    vida = 1;
}
Canoa::~Canoa(){}
void Canoa::setVida(int vida){
    this -> vida = vida;
}
int Canoa::getVida(){
    return vida;
}

bool Canoa::ataque(int x, int y){
    int vida = getVida();
    bool retorno;
    if(vida == 0){
        printf("Você já destruiu uma canoa que estava nessa posição.\nMais sorte na proxima rodada\n");
        retorno =  false;
    }
    else{
        vida--;
        setVida(vida);
        printf("!!!!O seu torpedo destruiu uma canoa!!!!\n");
        retorno = true;
    }
    scanf("%*c%*c");
    return retorno;
}