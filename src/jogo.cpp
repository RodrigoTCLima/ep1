#include "jogo.hpp"
Jogo::Jogo(){}
void Jogo::playGame(){
	cout << "\033[2J\033[1;1H";
	string nome;	
	cout << "Nome do Jogador1:";
	getline(cin, nome);
	Player jogador1(nome);
	cout << "Nome do Jogador2:";
	getline(cin, nome);
	Player jogador2(nome);
	cout << "\033[2J\033[1;1H";
	cout << "Ladies and Gentlemen, neste dia auspicioso teremos um grande confronto entre:\n" << jogador1.getNome() << ", O(a) destruidor(a) de navios\n\nvs\n\n" << jogador2.getNome() << ", O(a) aniquilador(a) de frotas\n\n\nPressione ENTER\n";
	scanf("%*c");
	vector<vector<Canoa*>> canoa1(13, vector<Canoa*>(13, NULL));
	vector<vector<Canoa*>> canoa2(13, vector<Canoa*>(13, NULL));
	vector<vector<Submarino*>> submarino1(13, vector<Submarino*>(13, NULL));
	vector<vector<Submarino*>> submarino2(13, vector<Submarino*>(13, NULL));
	vector<vector<PA*>> pa1(13, vector<PA*>(13, NULL));
	vector<vector<PA*>> pa2(13, vector<PA*>(13, NULL));
	Canoa lc1[12];
	Canoa lc2[12];
	Submarino ls1[12];
	Submarino ls2[12];
	PA lp1[12];
	PA lp2[12];
	Mapa m1;
	Mapa m2;
	int x, y, x2, x3, x4, y2, y3, y4;
	string tipo, posicao, inicial, resto;
	bool li1 = false;
	bool vez = true;	
	//Leitura inicial do arquivo;
	cout << "\033[2J\033[1;1H";
	cout << "Escolha um mapa de 1 - 3:";
	int maparq;
	cin >> maparq;
	ifstream arq;
	if(maparq == 1){
		arq.open ("doc/map_1.txt");
	}
	else if(maparq == 2){
		arq.open ("doc/map_2.txt");
	}
	else if(maparq == 3){
		arq.open ("doc/map_3.txt");
	}
  	while(arq >> inicial){
		if(inicial[0] >= 48 && inicial[0] <= 57){
			x = stoi(inicial);
			arq >> y >> tipo >> posicao;
			if(!li1){
				if(tipo == "canoa"){
					Canoa canoa(y, x, tipo, posicao);
					lc1[0] = canoa;
					if((x > 12) ||(x < 0) ||(y > 12) ||(y < 0)){
						lc1[0].~Canoa();
					}
					else{
						canoa1[y][x] = &lc1[0]; 
					}
				}
				else if(tipo == "submarino"){
					Submarino submarino(x,y,tipo,posicao);
					ls1[0] = submarino;
					if((ls1[0].getCooX2() > 12) || (ls1[0].getCooX2() < 0) || (ls1[0].getCooY2() > 12) || (ls1[0].getCooY2() < 0) || (x > 12) || (x < 0) || (y > 12) || (y<0)){
						ls1[0].~Submarino();
					}
					else{
						submarino1[y][x] = &ls1[0];
						x2 = ls1[0].getCooY2();
						y2 = ls1[0].getCooX2();
						submarino1[x2][y2] = &ls1[0];
					}
				}
				else if(tipo == "porta-avioes"){
					PA pa(x,y,tipo,posicao);
					lp1[0] = pa;
					if((lp1[0].getCooX2() > 12) || (lp1[0].getCooX2() < 0) || (lp1[0].getCooY2() > 12) || (lp1[0].getCooY2() < 0) || (x > 12) || (x < 0) || (y > 12) || (y<0) || (lp1[0].getCooX3() > 12) || (lp1[0].getCooX3() < 0) || (lp1[0].getCooY3() > 12) || (lp1[0].getCooY3() < 0) || (lp1[0].getCooX4() > 12) || (lp1[0].getCooX4() < 0) || (lp1[0].getCooY4() > 12) || (lp1[0].getCooY4() < 0) ){
						lp1[0].~PA();
					}
					else{
						pa1[y][x] = &lp1[0];
						x2 = lp1[0].getCooY2();
						y2 = lp1[0].getCooX2();
						pa1[x2][y2] = &lp1[0];
						x3 = lp1[0].getCooY3();
						y3 = lp1[0].getCooX3();
						pa1[x3][y3] = &lp1[0];
						x4 = lp1[0].getCooY4();
						y4 = lp1[0].getCooX4();
						pa1[x4][y4] = &lp1[0];
					}
				}
				for(int i = 1; i<12; i++){
					arq >> x >> y >> tipo >> posicao;
					if(tipo == "canoa"){
						Canoa canoa(y, x, tipo, posicao);
						lc1[i] = canoa; 
						if((x > 12) ||(x < 0) ||(y > 12) ||(y < 0)){
							lc1[i].~Canoa();
						}
						else{
							if((pa1[y][x] == NULL) && (canoa1[y][x] == NULL) && (submarino1[y][x] == NULL)){
								canoa1[y][x] = &lc1[i]; 
							}
						}
					}
					else if(tipo == "submarino"){
						Submarino submarino(x,y,tipo,posicao);
						ls1[i] = submarino;
						if((ls1[i].getCooX2() > 12) || (ls1[i].getCooX2() < 0) || (ls1[i].getCooY2() > 12) || (ls1[i].getCooY2() < 0) || (x > 12) || (x < 0) || (y > 12) || (y<0)){
							ls1[i].~Submarino();
						}
						else{
							x2 = ls1[i].getCooY2();
							y2 = ls1[i].getCooX2();
							if((pa1[y][x] == NULL) && (canoa1[y][x] == NULL) && (submarino1[y][x] == NULL) && (pa1[x2][y2] == NULL) && (canoa1[x2][y2] == NULL) && (submarino1[x2][y2] == NULL)){
								submarino1[y][x] = &ls1[i];
								submarino1[x2][y2] = &ls1[i];
							}
						}
					}
					else if(tipo == "porta-avioes"){
						PA pa(x,y,tipo,posicao);
						lp1[i] = pa;
						if((lp1[i].getCooX2() > 12) || (lp1[i].getCooX2() < 0) || (lp1[i].getCooY2() > 12) || (lp1[i].getCooY2() < 0) || (x > 12) || (x < 0) || (y > 12) || (y<0) || (lp1[i].getCooX3() > 12) || (lp1[i].getCooX3() < 0) || (lp1[i].getCooY3() > 12) || (lp1[i].getCooY3() < 0) || (lp1[i].getCooX4() > 12) || (lp1[i].getCooX4() < 0) || (lp1[i].getCooY4() > 12) || (lp1[i].getCooY4() < 0) ){
						lp1[i].~PA();
						}
						else{
							
							x2 = lp1[i].getCooY2();
							y2 = lp1[i].getCooX2();
							x3 = lp1[i].getCooY3();
							y3 = lp1[i].getCooX3();
							x4 = lp1[i].getCooY4();
							y4 = lp1[i].getCooX4();
							if((pa1[y][x] == NULL) && (canoa1[y][x] == NULL) && (submarino1[y][x] == NULL) && (pa1[x2][y2] == NULL) && (canoa1[x2][y2] == NULL) && (submarino1[x2][y2] == NULL) && (pa1[x3][y3] == NULL) && (canoa1[x3][y3] == NULL) && (submarino1[x3][y3] == NULL) && (pa1[x4][y4] == NULL) && (canoa1[x4][y4] == NULL) && (submarino1[x4][y4] == NULL)){
								pa1[y][x] = &lp1[i];
								pa1[x2][y2] = &lp1[i];
								pa1[x3][y3] = &lp1[i];
								pa1[x4][y4] = &lp1[i];
							}
						}
					}
				}
				li1 = true;
			}
			else{
				if(tipo == "canoa"){
					Canoa canoa(y, x, tipo, posicao);
					lc2[0] = canoa; 
					if((x > 12) ||(x < 0) ||(y > 12) ||(y < 0)){
						lc2[0].~Canoa();
					}
					else{
						canoa2[y][x] = &lc2[0]; 
					}
				}
				else if(tipo == "submarino"){
					Submarino submarino(x,y,tipo,posicao);
					ls2[0] = submarino;
					if((ls2[0].getCooX2() > 12) || (ls2[0].getCooX2() < 0) || (ls2[0].getCooY2() > 12) || (ls2[0].getCooY2() < 0) || (x > 12) || (x < 0) || (y > 12) || (y<0)){
						ls2[0].~Submarino();
					}
					else{
						submarino2[y][x] = &ls2[0];
						x2 = ls2[0].getCooY2();
						y2 = ls2[0].getCooX2();
						submarino2[x2][y2] = &ls2[0];
					}
				}
				else if(tipo == "porta-avioes"){
					PA pa(x,y,tipo,posicao);
					lp2[0] = pa;
					if((lp2[0].getCooX2() > 12) || (lp2[0].getCooX2() < 0) || (lp2[0].getCooY2() > 12) || (lp2[0].getCooY2() < 0) || (x > 12) || (x < 0) || (y > 12) || (y<0) || (lp2[0].getCooX3() > 12) || (lp2[0].getCooX3() < 0) || (lp2[0].getCooY3() > 12) || (lp2[0].getCooY3() < 0) || (lp2[0].getCooX4() > 12) || (lp2[0].getCooX4() < 0) || (lp2[0].getCooY4() > 12) || (lp2[0].getCooY4() < 0) ){
						lp2[0].~PA();
					}
					else{
						pa2[y][x] = &lp2[0];
						x2 = lp2[0].getCooY2();
						y2 = lp2[0].getCooX2();
						pa2[x2][y2] = &lp2[0];
						x3 = lp2[0].getCooY3();
						y3 = lp2[0].getCooX3();
						pa2[x3][y3] = &lp2[0];
						x4 = lp2[0].getCooY4();
						y4 = lp2[0].getCooX4();
						pa2[x4][y4] = &lp2[0];
					}
				}
				for(int i = 1; i<12; i++){
					arq >> x >> y >> tipo >> posicao;
					if(tipo == "canoa"){
						Canoa canoa(y, x, tipo, posicao);
						lc2[i] = canoa;
						if((x > 12) ||(x < 0) ||(y > 12) ||(y < 0)){
							lc2[i].~Canoa();
						}
						else{
							if((pa2[y][x] == NULL) && (canoa2[y][x] == NULL) && (submarino2[y][x] == NULL)){
								canoa2[y][x] = &lc2[i];
							} 
						}
					}
					else if(tipo == "submarino"){
						Submarino submarino(x,y,tipo,posicao);
						ls2[i] = submarino;
						if((ls2[0].getCooX2() > 12) || (ls2[0].getCooX2() < 0) || (ls2[0].getCooY2() > 12) || (ls2[0].getCooY2() < 0) || (x > 12) || (x < 0) || (y > 12) || (y<0)){
							ls2[0].~Submarino();
						}
						else{
							x2 = ls2[i].getCooY2();
							y2 = ls2[i].getCooX2();
							if((pa2[y][x] == NULL) && (canoa2[y][x] == NULL) && (submarino2[y][x] == NULL) && (pa2[x2][y2] == NULL) && (canoa2[x2][y2] == NULL) && (submarino2[x2][y2] == NULL)){
								submarino2[y][x] = &ls2[i];
								submarino2[x2][y2] = &ls2[i];
							}
						}
					}
					else if(tipo == "porta-avioes"){
						PA pa(x,y,tipo,posicao);
						lp2[i] = pa;
						if((lp2[i].getCooX2() > 12) || (lp2[i].getCooX2() < 0) || (lp2[i].getCooY2() > 12) || (lp2[i].getCooY2() < 0) || (x > 12) || (x < 0) || (y > 12) || (y<0) || (lp2[i].getCooX3() > 12) || (lp2[i].getCooX3() < 0) || (lp2[i].getCooY3() > 12) || (lp2[i].getCooY3() < 0) || (lp2[i].getCooX4() > 12) || (lp2[i].getCooX4() < 0) || (lp2[i].getCooY4() > 12) || (lp2[i].getCooY4() < 0) ){
							lp2[i].~PA();
						}
						else{
							x2 = lp2[i].getCooY2();
							y2 = lp2[i].getCooX2();
							x3 = lp2[i].getCooY3();
							y3 = lp2[i].getCooX3();
							x4 = lp2[i].getCooY4();
							y4 = lp2[i].getCooX4();
							if((pa2[y][x] == NULL) && (canoa2[y][x] == NULL) && (submarino2[y][x] == NULL) && (pa2[x2][y2] == NULL) && (canoa2[x2][y2] == NULL) && (submarino2[x2][y2] == NULL) && (pa2[x3][y3] == NULL) && (canoa2[x3][y3] == NULL) && (submarino2[x3][y3] == NULL) && (pa2[x4][y4] == NULL) && (canoa2[x4][y4] == NULL) && (submarino2[x4][y4] == NULL)){
								pa2[y][x] = &lp2[i];
								pa2[x2][y2] = &lp2[i];
								pa2[x3][y3] = &lp2[i];
								pa2[x4][y4] = &lp2[i];
							}						
						}
					}
				}
			}	
		}
		else{
			getline(arq, resto); 			
		}
	}
	arq.close();
	//começo dos ataques;
	while(1){
		bool destruido = false;
		if(jogador1.getPontuacao() == 12 || jogador2.getPontuacao() == 12){
			GameOver(jogador1, jogador2);
			scanf("%*c%*c%*c");
			break;
		}
		else if(vez){
			jogador1.setTentativas(jogador1.getTentativas() + 1);
			m2.mostrarMapa();
			cout << endl << "Pontuação atual: "<< jogador1.getPontuacao() <<"/12";
			cout << endl << jogador1.getNome();
			cout << ", onde você que atacar?(Ex.: 11 10): ";
			cin >> x >> y;	
			if((x > 12) || (x < 0) ||(y > 12) || (y < 0)){
				printf("Opção de ataque inválida!\nVocê perdeu a vez!\n");
				scanf("%*c%*c");
				vez = false;
				continue;
			}
			if(canoa2[y][x] != NULL){
				destruido = canoa2[y][x][0].ataque(y, x); 
				m2.setCPA(y, x);
				if(destruido){
					jogador1.setPontuacao(jogador1.getPontuacao()+1);
					jogador1.setAcertos(jogador1.getAcertos() + 1);
				}
				
			}
			else if(submarino2[y][x] != NULL){
				int vi1, vf1, vf2;
				vi1 = submarino2[y][x][0].getVida();
				destruido = submarino2[y][x][0].ataque(y, x);
				vf1 = submarino2[y][x][0].getVida();
				vf2 = submarino2[y][x][0].getVida2();
				if(vi1 == vf1){
					if(vf2 == 1){
						m2.setSub(y, x);
						jogador1.setAcertos(jogador1.getAcertos() + 1);
					}
					else{
						m2.setCPA(y, x);
						jogador1.setAcertos(jogador1.getAcertos() + 1);
					}
				}
				else{
					if(vf1 == 1){
						m2.setSub(y, x);
						jogador1.setAcertos(jogador1.getAcertos() + 1);
					}
					else{
						m2.setCPA(y, x);
						jogador1.setAcertos(jogador1.getAcertos() + 1);
					}
				}
				if(destruido){
					jogador1.setPontuacao(jogador1.getPontuacao()+1);
				}
				
			}
			else if(pa2[y][x] != NULL){
				jogador1.setAcertos(jogador1.getAcertos() + 1);
				int vi, vf;
				vi = pa2[y][x][0].getVida2() + pa2[y][x][0].getVida3() + pa2[y][x][0].getVida4() + pa2[y][x][0].getVida();
				destruido = pa2[y][x][0].ataque(y, x);
				vf = pa2[y][x][0].getVida2() + pa2[y][x][0].getVida3() + pa2[y][x][0].getVida4() + pa2[y][x][0].getVida();
				if(vi != vf){
					m2.setCPA(y, x);
					jogador1.setAcePA(jogador1.getAcePA()+1);
				}
				else{
					jogador1.setFalPA(jogador1.getFalPA() + 1);
				}
				if(destruido){
					jogador1.setPontuacao(jogador1.getPontuacao()+1);
				}
			}
			else{
				
				m2.setVazio(y, x);
				printf("Vocẽ errou!!\nMais sorte na próxima.\n");
				scanf("%*c%*c");
			}
			m2.mostrarMapa();
			scanf("%*c");
			vez = false;
		}
		else{
			jogador2.setTentativas(jogador2.getTentativas() + 1);
			m1.mostrarMapa();
			cout << endl << "Pontuação atual: "<< jogador2.getPontuacao()<<"/12";;
			cout << endl << jogador2.getNome();
			cout << ", onde você que atacar?(Ex.: 11 10): ";
			cin >> x >> y;	
			if((x > 12) || (x < 0) ||(y > 12) || (y < 0)){
				printf("Opção de ataque inválida!\nVocê perdeu a vez!\n");
				scanf("%*c%*c");
				vez = true;
				continue;
			}
			if(canoa1[y][x] != NULL){
				destruido = canoa1[y][x][0].ataque(y, x);
				m1.setCPA(y, x);
				if(destruido){
					jogador2.setPontuacao(jogador2.getPontuacao()+1);
					jogador2.setAcertos(jogador2.getAcertos() + 1);
				}
				
			}
			else if(submarino1[y][x] != NULL){
				int vi1, vf1, vf2;
				vi1 = submarino1[y][x][0].getVida();
				destruido = submarino1[y][x][0].ataque(y, x);
				vf1 = submarino1[y][x][0].getVida();
				vf2 = submarino1[y][x][0].getVida2();
				if(vi1 == vf1){
					if(vf2 == 1){
						m1.setSub(y, x);
						jogador2.setAcertos(jogador2.getAcertos() + 1);
					}
					else{
						m1.setCPA(y, x);
						jogador2.setAcertos(jogador2.getAcertos() + 1);
					}
				}
				else{
					if(vf1 == 1){
						m1.setSub(y, x);
						jogador2.setAcertos(jogador2.getAcertos() + 1);
					}
					else{
						m1.setCPA(y, x);
						jogador2.setAcertos(jogador2.getAcertos() + 1);
					}
				}
				if(destruido){
					jogador2.setPontuacao(jogador2.getPontuacao()+1);
				}
				jogador2.setAcertos(jogador2.getAcertos() + 1);
			}
			else if(pa1[y][x] != NULL){
				jogador2.setAcertos(jogador2.getAcertos() + 1);
				int vi, vf;
				vi = pa1[y][x][0].getVida2() + pa1[y][x][0].getVida3() + pa1[y][x][0].getVida4() + pa1[y][x][0].getVida();
				destruido = pa1[y][x][0].ataque(y, x);
				vf = pa1[y][x][0].getVida2() + pa1[y][x][0].getVida3() + pa1[y][x][0].getVida4() + pa1[y][x][0].getVida();
				if(vi != vf){
					m1.setCPA(y, x);
					jogador2.setAcePA(jogador2.getAcePA() + 1);	
				}
				else{
					jogador2.setFalPA(jogador2.getFalPA() + 1);
				}
				if(destruido){
					jogador2.setPontuacao(jogador2.getPontuacao()+1);
				}
			}
			else{
				m1.setVazio(y, x);
				printf("Vocẽ errou!!\nMais sorte na próxima.\n");
				scanf("%*c%*c");
			}
			m1.mostrarMapa();
			scanf("%*c");
			vez = true;
		}

	}	
}
Jogo::~Jogo(){}
void Jogo::GameOver(Player x, Player y){
	if(x.getPontuacao()>y.getPontuacao()){
		cout << "Vejam só parece que agora o(a) ";
		cout << x.getNome() << " tornou o(a) super destruidor(a) de navios\n"; 
	}
	else{
		cout << "Vejam só parece que agora o(a) ";
		cout << y.getNome() << " tornou o(a) super aniquilador(a) de frotas\n";
	}
	printf("\nESTATÍSTICAS:\n");
	cout << x.getNome() <<": " << endl;
	cout << "Quantidade de tentativas:                         " << x.getTentativas() << endl;
	cout << "Quantidade de acertos:                            " << x.getAcertos() << endl;
	cout << "Quantidade de ataques efetivos a porta-aviões:    " << x.getAcePA() << endl;
	cout << "Quantidade de ataques desviados por porta-aviões: "<<  x.getFalPA() << endl;
	
	
	cout << y.getNome() <<": " << endl;
	cout << "Quantidade de tentativas:                         " << y.getTentativas() << endl;
	cout << "Quantidade de acertos:                            " << y.getAcertos() << endl;
	cout << "Quantidade de ataques efetivos a porta-aviões:    " << y.getAcePA() << endl;
	cout << "Quantidade de ataques desviados por porta-aviões: "<<  y.getFalPA() << endl;
	
	scanf("%*c");

}