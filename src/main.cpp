#include <fstream>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include "player.hpp"
#include "canoa.hpp"
#include "pa.hpp"
#include "embarcacao.hpp"
#include "submarino.hpp"
#include "menu.hpp"
#include "mapa.hpp"
#include "jogo.hpp"
using namespace std;

int main(){
    Menu menu;
    int a = menu.telaInicial();
    char Enter;
    while(1){
        if(a == 1){
            Jogo jogo1;
            jogo1.playGame();
        }
        else if(a == 2){
            a = menu.mostrarInstrucoes();
            if(a == 2){
                cout << "\033[2J\033[1;1H";
                return 0;
            }
        }
        else if(a == 3){
            a = menu.mostrarCreditos();
            if(a == 2){
                cout << "\033[2J\033[1;1H";
                return 0;
            }
        }
        else if(a == 0){ cout << "\033[2J\033[1;1H"; return 0; }
        Menu menu2;
        a = menu2.mostrarMenu();
    }
    return 0;
}
