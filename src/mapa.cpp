#include "mapa.hpp"
#include <iostream>
#include <vector>
Mapa::Mapa(){
	for(int i =0; i< 13; i++){
		for(int j = 0; j<13;j++ ){
			mapa[i].push_back('~');
		}
	}
}
Mapa::~Mapa(){}

void Mapa::mostrarMapa(){
	cout << "\033[2J\033[1;1H";
	for(int i=12; i>=-1;i--){
		if(i == -1){
			printf("\n        0   1   2   3   4   5   6   7   8   9   10  11  12\n");
		}
		else{
			if(i > 9){
				for(int j=0; j<13; j++){
					if(j == 0){
						printf("%d     %c%c%c",i,mapa[i][j],mapa[i][j],mapa[i][j]);
					}
					else{
						printf(" %c%c%c", mapa[i][j],mapa[i][j],mapa[i][j]);
					}
				}
			}
			else{
				for(int j=0; j<13; j++){
					if(j == 0){
						printf("%d      %c%c%c",i,mapa[i][j],mapa[i][j],mapa[i][j]);
					}
					else{
						printf(" %c%c%c", mapa[i][j],mapa[i][j],mapa[i][j]);
					}
				}
			}

			printf("\n");
		}
	}
}

void Mapa::setVazio(int x, int y){
	this-> mapa[x][y] = 'X';	
}
void Mapa::setSub(int x, int y){
	this-> mapa[x][y] = '#';	
}
void Mapa::setCPA(int x, int y){
	this-> mapa[x][y] = '@';
}