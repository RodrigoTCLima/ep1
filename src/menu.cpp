#include "menu.hpp"
Menu::Menu(){}
Menu::~Menu(){}
int Menu::telaInicial(){
	char s;
	cout << "\033[2J\033[1;1H";
	printf("                                                                                                                                     \n");                                                                                                                                                                                                                                                                                                                                                                                                                
	printf("      .@@@@@@,   @@@@;   @@@@@@@@  `@@@@`   @@@@@:    @@@`@@@+  @@@@.            +@@, .@@@' @@@@;  @@@@  @@@@ `@@@@`   @@@@@:        \n");
	printf("       `@@@@@@@   #@@@#   @@@@@@@@  `@@@@:   +@@@#     @@+ #@@`  @@@@+            ;@@@ .@@@  #@@@#  #@@'  #@@; `@@@@:   +@@@#        \n");
	printf("        ;@   `@    @@@@  `@. @+ #@    @#@+    ,@`      @'   @;    @@@@             @@@`  @`   @@@@   @@   .@`    @#@+    ,@`         \n");
	printf("        #@   @@   .@`+@  .@  @, @;   '@ @@    ;@       @,   @.   :@ #@             @;@+ :@   .@`+@   @@   @+    '@ @@    ;@          \n");
	printf("        @@@@@@    @@ :@`    .@       @, @@    @@      `@@@@@@    @' +@             @ @@ #@   @@ :@`  ;@` '@     @, @@    @@          \n");
	printf("        @@@@@@@  ;@@@@@,    '@      #@@@@@`   @'      '@@@@@@   #@@@@@.           :@ .@;@@  ;@@@@@,  ,@. @:    #@@@@@`   @'          \n");
	printf("       .@    +@  @@@@@@#    #@     .@@@@@@,   @,  `@. @#   @#   @@@@@@'           +@  @@@;  @@@@@@#   @+@@    .@@@@@@,   @,  `@.     \n");
	printf("       ;@    #@ +@    @@    @+     @#   `@'  .@`  ,@  @+   @'  #@    @#           @@  '@@  +@    @@   @@@`    @#   `@'  .@`  ,@      \n");
	printf("      @@@@@@@@ @@@@ `@@@@ @@@@@, `@@@+ ;@@@@@@@@@@@@ @@@':@@@,@@@# ,@@@+         @@@@  @@ @@@@ `@@@@  #@#   `@@@+ ;@@@@@@@@@@@@      \n");
	printf("      #@@@@@;  @@@: `@@@. @@@@#  `@@@` ,@@@.@@@@@@@# @@@`,@@# @@@. .@@@`         #@@;  #@ @@@: `@@@.  '@    `@@@` ,@@@.@@@@@@@#      \n");
	printf("                                                                                                                                     \n");
	printf("                                                                                                               by: Rodrigo Lima      \n");
	printf("                                                                                                                                     \n");
	printf("                                               Pressione a tecla ENTER para prosseguir                                               \n");
	scanf("%c", &s);
	int a = mostrarMenu();
	
	if(a == 0){
		return 0;
	}
	else if(a == 1){
		return 1;
	}
	else if(a == 2){
		return 2;
	}
	else if(a == 3){
		return 3;
	}
}
int Menu::mostrarMenu(){
	char opcao;
	cout << "\033[2J\033[1;1H";
	printf("                                                                                                                                     \n");                                                                                                                                                                                                                                                                                                                                                                                                                
	printf("      .@@@@@@,   @@@@;   @@@@@@@@  `@@@@`   @@@@@:    @@@`@@@+  @@@@.            +@@, .@@@' @@@@;  @@@@  @@@@ `@@@@`   @@@@@:        \n");
	printf("       `@@@@@@@   #@@@#   @@@@@@@@  `@@@@:   +@@@#     @@+ #@@`  @@@@+            ;@@@ .@@@  #@@@#  #@@'  #@@; `@@@@:   +@@@#        \n");
	printf("        ;@   `@    @@@@  `@. @+ #@    @#@+    ,@`      @'   @;    @@@@             @@@`  @`   @@@@   @@   .@`    @#@+    ,@`         \n");
	printf("        #@   @@   .@`+@  .@  @, @;   '@ @@    ;@       @,   @.   :@ #@             @;@+ :@   .@`+@   @@   @+    '@ @@    ;@          \n");
	printf("        @@@@@@    @@ :@`    .@       @, @@    @@      `@@@@@@    @' +@             @ @@ #@   @@ :@`  ;@` '@     @, @@    @@          \n");
	printf("        @@@@@@@  ;@@@@@,    '@      #@@@@@`   @'      '@@@@@@   #@@@@@.           :@ .@;@@  ;@@@@@,  ,@. @:    #@@@@@`   @'          \n");
	printf("       .@    +@  @@@@@@#    #@     .@@@@@@,   @,  `@. @#   @#   @@@@@@'           +@  @@@;  @@@@@@#   @+@@    .@@@@@@,   @,  `@.     \n");
	printf("       ;@    #@ +@    @@    @+     @#   `@'  .@`  ,@  @+   @'  #@    @#           @@  '@@  +@    @@   @@@`    @#   `@'  .@`  ,@      \n");
	printf("      @@@@@@@@ @@@@ `@@@@ @@@@@, `@@@+ ;@@@@@@@@@@@@ @@@':@@@,@@@# ,@@@+         @@@@  @@ @@@@ `@@@@  #@#   `@@@+ ;@@@@@@@@@@@@      \n");
	printf("      #@@@@@;  @@@: `@@@. @@@@#  `@@@` ,@@@.@@@@@@@# @@@`,@@# @@@. .@@@`         #@@;  #@ @@@: `@@@.  '@    `@@@` ,@@@.@@@@@@@#      \n");
	printf("                                                                                                                                     \n");
	printf("                                                                                                                                     \n");
	printf("                         (1)Jogar                (2)Instruções                (3)Créditos                (4)Sair                     \n");
	printf("                                                                                                                                     \n");
	cout << "                                Pressione o número da opção da sua escolha e aperte enter: ";
	cin >> opcao;
	scanf("%*c");
	if(opcao == '1'){
		return 1;
	}
	else if(opcao == '2'){
		return 2;
	}
	else if(opcao == '3'){
		return 3;
	}
	else if(opcao == '4'){
		return 0;
	}
	else{
		return mostrarMenuInv();
	}
}
int Menu::mostrarMenuInv(){
	char opcao;
	cout << "\033[2J\033[1;1H";
	printf("                                                                                                                                     \n");                                                                                                                                                                                                                                                                                                                                                                                                                
	printf("      .@@@@@@,   @@@@;   @@@@@@@@  `@@@@`   @@@@@:    @@@`@@@+  @@@@.            +@@, .@@@' @@@@;  @@@@  @@@@ `@@@@`   @@@@@:        \n");
	printf("       `@@@@@@@   #@@@#   @@@@@@@@  `@@@@:   +@@@#     @@+ #@@`  @@@@+            ;@@@ .@@@  #@@@#  #@@'  #@@; `@@@@:   +@@@#        \n");
	printf("        ;@   `@    @@@@  `@. @+ #@    @#@+    ,@`      @'   @;    @@@@             @@@`  @`   @@@@   @@   .@`    @#@+    ,@`         \n");
	printf("        #@   @@   .@`+@  .@  @, @;   '@ @@    ;@       @,   @.   :@ #@             @;@+ :@   .@`+@   @@   @+    '@ @@    ;@          \n");
	printf("        @@@@@@    @@ :@`    .@       @, @@    @@      `@@@@@@    @' +@             @ @@ #@   @@ :@`  ;@` '@     @, @@    @@          \n");
	printf("        @@@@@@@  ;@@@@@,    '@      #@@@@@`   @'      '@@@@@@   #@@@@@.           :@ .@;@@  ;@@@@@,  ,@. @:    #@@@@@`   @'          \n");
	printf("       .@    +@  @@@@@@#    #@     .@@@@@@,   @,  `@. @#   @#   @@@@@@'           +@  @@@;  @@@@@@#   @+@@    .@@@@@@,   @,  `@.     \n");
	printf("       ;@    #@ +@    @@    @+     @#   `@'  .@`  ,@  @+   @'  #@    @#           @@  '@@  +@    @@   @@@`    @#   `@'  .@`  ,@      \n");
	printf("      @@@@@@@@ @@@@ `@@@@ @@@@@, `@@@+ ;@@@@@@@@@@@@ @@@':@@@,@@@# ,@@@+         @@@@  @@ @@@@ `@@@@  #@#   `@@@+ ;@@@@@@@@@@@@      \n");
	printf("      #@@@@@;  @@@: `@@@. @@@@#  `@@@` ,@@@.@@@@@@@# @@@`,@@# @@@. .@@@`         #@@;  #@ @@@: `@@@.  '@    `@@@` ,@@@.@@@@@@@#      \n");
	printf("                                                                                                                                     \n");
	printf("                                                                                                                                     \n");
	printf("                         (1)Jogar                (2)Instruções                (3)Créditos                (4)Sair                     \n");
	printf("                                                                                                                                     \n");
	printf("                                                                                                                                     \n");
	cout << "                                                        Opção inválida.\n                                Pressione APENAS O NÚMERO da opção da sua escolha e aperte enter: ";
	cin >> opcao;
	scanf("%*c");
	if(opcao == '1'){
		return 1;
	}
	else if(opcao == '2'){
		return 2;
	}
	else if(opcao == '3'){
		return 3;
	}
	else if(opcao == '4'){
		return 0;
	}
	else{
		return mostrarMenuInv();
	}
}

int Menu::mostrarInstrucoes(){
	char Enter;
	cout << "\033[2J\033[1;1H";
    cout <<"                                    Instruções:\n";
	cout <<"1. Neste jogo haverá 2 jogadores, um contra o outro.\n";
	cout <<"2. Cada jogador terá doze embarcações.\n";
	cout <<"3. Cada embarcação terá uma classicação entre as três possíveis (canoa, submarino, porta-aviões):\n";
	cout <<"   3.1. Canoa -> ocupa um espaço no mapa e não tem habilidades especiais.\n";
	cout <<"   3.2. Submario -> ocupa dois espaços no mapa e precisa ser atacado duas vezes em cada espaço para ser destruido.\n";
	cout <<"   3.3. Porta-aviões -> ocupa quatro espaços no mapa e tem 50% de chance se de defender de a cada ataque.\n";
	cout <<"4. Cada embarcação terá uma orientação (cima, baixo, esquerda, direita), exceto a canoa que teria como orientação a palavra 'nada'\n";
	cout <<"5. Cada embarcação deverá ser colocada seguindo o seguinte padrão:\n";
	cout <<"   5.1. A coordenada x, a coordenada y, a classificação da embarcação e a orientação da embarcação. Exemplo: 2 3 canoa nada.\n";
	cout <<"6. Cada jogador terá um mapa 13X13 (0 - 12) X (0 - 12).\n";
	cout <<"7. Ao realizar o ataque, o jogador deverá colocar o número da coluna e o número da linha (separados por um espaço) que deseja atacar.\n";
	cout <<"   7.1. O jogador que tentar atacar quaisquer coordenadas fora dos limites estipulados, perderá a vez.\n";
	cout <<"8. O jogador que tentar colocar uma embarcação fora dos limites estipulados (0 - 12) X (0 - 12), ficará sem essa embarcação.\n";
	cout <<"9. O jogador que tentar colocar uma embarcação em cima de outra, ficará sem a mais nova entre as duas.\n";
	cout <<"10. Ganha o jogador que destruir as 12 embarcações inimigas primeiro.\n";
	cout <<"11. Ganha muito mais quem se diverte.\n";
    
	cout << "\n\nSelecione:           (1)Voltar ao menu.            (2)Sair.\n";
	scanf("%c", &Enter);
	scanf("%*c");
	while(1){
		if(Enter - '0' > 0 && Enter - '0' <3){			
			return Enter - '0';
		}
		else{
			printf("Opção inválida, tente novamente\n");
			scanf("%c", &Enter);
			scanf("%*c");
			if(Enter - '0' > 0 && Enter - '0' <3){			
				return Enter - '0';
			}
		}
	}
}
int Menu::mostrarCreditos(){
	cout << "\033[2J\033[1;1H";
    char Enter;
	cout << "                                    CRÉDITOS:\n";
	cout << "Único e principal programador: Rodrigo Tiago Costa Lima (rodrigotiagocl@gmail.com).\n";
    cout << "\n\nSelecione:           1.Voltar ao menu.            2.Sair.\n";
    scanf("%c", &Enter);
	scanf("%*c");
	while(1){
		if(Enter - '0' > 0 && Enter - '0' <3){			
			return Enter - '0';
		}
		else{
			printf("Opção inválida, tente novamente\n");
			scanf("%c", &Enter);
			scanf("%*c");
			if(Enter - '0' > 0 && Enter - '0' <3){			
				return Enter - '0';
			}
		}
	}
}
