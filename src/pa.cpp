#include "pa.hpp"

PA::PA(int x, int y, string tipo, string posicao){
    setCooX(x);
    setCooY(y);
    setTipo(tipo);
    setPosicao(posicao);
    setVida(1);
    setVida2(1);
    setVida3(1);
    setVida4(1);
    int x2, y2, x3, y3, x4, y4;
    if(posicao== "esquerda"){
        x2 = x - 1;
        x3 = x - 2;
        x4 = x - 3;
        y2 = y + 0;
        y3 = y + 0;
        y4 = y + 0;
    }
    else if(posicao == "direita"){
        x2 = x + 1;
        x3 = x + 2;
        x4 = x + 3;
        y2 = y + 0;
        y3 = y + 0;
        y4 = y + 0;
    }
    else if(posicao == "cima"){
        x2 = x + 0;
        x3 = x + 0;
        x4 = x + 0;
        y2 = y+1;
        y3 = y+2;
        y4 = y+3;
    }
    else if(posicao == "baixo"){
        x2 = x + 0;
        x3 = x + 0;
        x4 = x + 0;
        y2 = y-1;
        y3 = y-2;
        y4 = y-3;
    }
    setCooX2(x2);
    setCooX3(x3);
    setCooX4(x4);
    setCooY2(y2);
    setCooY3(y3);
    setCooY4(y4);
    setDestruido(false);

}
PA::PA(){}
PA::~PA(){}
int PA::getCooX2(){
    return x2;
}
void PA::setCooX2(int x2){
    this-> x2 = x2;
}
int PA::getCooY2(){
    return y2;
}
void PA::setCooY2(int y2){
    this-> y2 = y2;
}

int PA::getCooX3(){
    return x3;
}
void PA::setCooX3(int x3){
    this-> x3 = x3;
}
int PA::getCooY3(){
    return y3;
}
void PA::setCooY3(int y3){
    this-> y3 = y3;
}
int PA::getCooX4(){
    return x4;
}
void PA::setCooX4(int x4){
    this-> x4 = x4;
}
int PA::getCooY4(){
    return y4;
}
void PA::setCooY4(int y4){
    this-> y4 = y4;
}
int PA::getVida(){
    return vida;
}
void PA::setVida(int vida){
    this-> vida = vida;
}
int PA::getVida2(){
    return vida2;
}
void PA::setVida2(int vida2){
    this->vida2 = vida2;
}
int PA::getVida3(){
    return vida3;
}
void PA::setVida3(int vida3){
    this->vida3 = vida3;
}
int PA::getVida4(){
    return vida4;
}
void PA::setVida4(int vida4){
    this->vida4 = vida4;
}
bool PA::getDestruido(){
    return destruido;
}
void PA::setDestruido(bool destruido){
    this->destruido = destruido;
}
bool PA::ataque(int X, int Y){
    srand(time(0));
    int chance = rand() % 1000000;
    if(chance < 500000){
        if(getCooX() == Y && getCooY() == X){
            int v = vida;
            v-=1;
			if(v<0 && (vida2 != 0 || vida3 != 0 || vida4 != 0)){
				printf("Você já destruiu essa parte do porta-aviões, entretando não o destruiu por completo.\nMais sorte na proxima rodada.\n");
			}
			else{
		        vida = v;
		        if(destruido){
		            printf("Você já destruiu um porta-aviões nessa coordenada. Mais sorte na próxima.\n");
                    destruido = false;
		        }
                else if((vida == 0) && (vida2 == 0) && (vida3 == 0) && (vida4 == 0)){
		            setDestruido(true);
		            printf("!!!!!!!!PARABÉNS!!!!!!!!\nVocê destruiu um Porta-Aviões inimigo\n");
		        }
		        else{
		            if((vida2 + vida3 + vida4) == 3){
		                printf("Você destruiu uma das partes de um porta-aviões, mas ainda restam 3 partes.\n");
		            }
		            else if((vida2 + vida3 + vida4) == 2){
		                printf("Você destruiu mais uma das partes de um porta-aviões, mas ainda restam 2 partes.\n");
		            }
		            else if((vida2 + vida3 + vida4) == 1){
		                printf("Você destruiu mais uma das partes de um porta-aviões, mas ainda resta 1 parte.\n");
		            }
		        }
            }
        }
        else if(x2 == Y && y2 == X){
            int v = vida2;
            v-=1;
            if(v<0 && (vida != 0 || vida3 != 0|| vida4 != 0)){
				printf("Você já destruiu essa parte do porta-aviões, entretando não o destruiu por completo.\nMais sorte na proxima rodada.\n");
			}
			else{
		        vida2 = v;
		        
		        if(destruido){
		            printf("Você já destruiu um porta-aviões nessa coordenada. Mais sorte na próxima.\n");
		        }
                else if((vida == 0) && (vida2 == 0) && (vida3 == 0) && (vida4 == 0)){
		            destruido = true;
		            printf("!!!!!!!!PARABÉNS!!!!!!!!\nVocê destruiu um Porta-Aviões inimigo\n");
		        }
		        else{
		            if((vida + vida3 +vida4) == 3){
		                printf("Você destruiu uma das partes de um porta-aviões, mas ainda restam 3 partes.\n");
		            }
		            else if((vida + vida3 +vida4) == 2){
		                printf("Você destruiu mais uma das partes de um porta-aviões, mas ainda restam 2 partes.\n");
		            }
		            else if((vida + vida3 +vida4) == 1){
		                printf("Você destruiu mais uma das partes de um porta-aviões, mas ainda resta 1 parte.\n");
		            }
		        }
            }
        }
        else if(x3 == Y && y3 == X){
            int v = vida3;
            v-=1;
            if(v<0 && (vida2 != 0 || vida != 0 || vida4 != 0)){
				printf("Você já destruiu essa parte do porta-aviões, entretando não o destruiu por completo.\nMais sorte na proxima rodada.\n");
			}
			else{
		        vida3 = v;
		        if(destruido){
		            printf("Você já destruiu um porta-aviões nessa coordenada. Mais sorte na próxima.\n");
                    destruido = false;
		        }
                else if((vida == 0) && (vida2 == 0) && (vida3 == 0) && (vida4 == 0)){
		            destruido = true;
		            printf("!!!!!!!!PARABÉNS!!!!!!!!\nVocê destruiu um Porta-Aviões inimigo\n");
		        }
		        else{
		            if((vida2 +vida+vida4) == 3){
		                printf("Você destruiu uma das partes de um porta-aviões, mas ainda restam 3 partes.\n");
		            }
		            else if((vida2+vida+vida4) == 2){
		                printf("Você destruiu mais uma das partes de um porta-aviões, mas ainda restam 2 partes.\n");
		            }
		            else if((vida2+vida+vida4) == 1){
		                printf("Você destruiu mais uma das partes de um porta-aviões, mas ainda resta 1 parte.\n");
		            }
		        }
            }
        }
        else if(x4  == Y && y4 == X){
            int v = vida4;
            v-=1;
            if(v<0 && (vida2!=0 || vida3!=0 || vida4 != 0)){
				printf("Você já destruiu essa parte do porta-aviões, entretando não o destruiu por completo.\nMais sorte na proxima rodada.\n");
			}
			else{
		        vida4 = v;
		        if((vida == 0) && (vida2 == 0) && (vida3 == 0) && (vida4 == 0)){
		            destruido = true;
		            printf("!!!!!!!!PARABÉNS!!!!!!!!\nVocê destruiu um Porta-Aviões inimigo\n");
		        }
		        else if(destruido){
		            printf("Você já destruiu um porta-aviões nessa coordenada. Mais sorte na próxima.\n");
                    destruido = false;
		        }
		        else{
		            if((vida2+vida3+vida) == 3){
		                printf("Você destruiu uma das partes de um porta-aviões, mas ainda restam 3 partes.\n");
		            }
		            else if((vida2+vida3+vida) == 2){
		                printf("Você destruiu mais uma das partes de um porta-aviões, mas ainda restam 2 partes.\n");
		            }
		            else if((vida2+vida3+vida) == 1){
		                printf("Você destruiu mais uma das partes de um porta-aviões, mas ainda resta 1 parte.\n");
		            }
		        }
            }
        }
        
    }
    else{
            printf("Torpedo destruido. Mais sorte na proxima.\n");
    }
    
    scanf("%*c%*c%*c");
    return destruido;
}
