#include "player.hpp"
Player::Player(string nome){
	setNome(nome);
	setPontuacao(0);
	setAcertos(0);
	setTentativas(0);
	setFalPA(0);
	setAcePA(0);
}
Player::~Player(){}
string Player::getNome(){
	return nome;
}
void Player::setNome(string nome){
	this-> nome = nome;
}
int Player::getPontuacao(){
	return pontuacao;
}
void Player::setPontuacao(int pontuacao){
	this-> pontuacao = pontuacao;
}
int Player::getAcertos(){
	return acertos;
}
void Player::setAcertos(int acertos){
	this-> acertos = acertos;
}
int Player::getTentativas(){
	return tentativas;
}
void Player::setTentativas(int tentativas){
	this-> tentativas = tentativas;
}


int Player::getAcePA(){
	return acePA;
}
void Player::setAcePA(int acePA){
	this-> acePA = acePA;
}


int Player::getFalPA(){
	return falPA;
}
void Player::setFalPA(int falPA){
	this-> falPA = falPA;
}
